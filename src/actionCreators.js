export function addSum(num) {
  return {
    type: 'ADD_SUM',
    payload: {
      num,
    },
  };
}
