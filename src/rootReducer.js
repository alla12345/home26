const initState = {
  count: 0,
  sum: 0,
};

function reducer(state = initState, action) {
  switch (action.type) {

    case 'ADD_SUM': {
      const { num } = action.payload;
      return { ...state, sum: state.sum + num };
    }

    default:
      return state;
  }
}

export default reducer;
